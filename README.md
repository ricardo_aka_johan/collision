# Collision

Collision est un caractère typographique dessiné par [Ricardo aka Johan](http://ricardoakajohan.be/)



## Specimen

![specimen1](Specimen1.jpg)
![specimen2](Specimen2.jpg)
![specimen3](Specimen3.jpg)
![specimen4](Specimen4.jpg)

## License

Collision is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL
